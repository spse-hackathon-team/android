package com.hackathon.elpresidente.app;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;

public class StatusActivity extends AppCompatActivity {

    TextView temp, humid, co2, motion, luminosity, statRoomName;

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_status);

        temp = (TextView) findViewById(R.id.statusRoomTemp);
        humid = (TextView) findViewById(R.id.statusRoomHumid);
        co2 = (TextView) findViewById(R.id.statusRoomCO2);
        motion = (TextView) findViewById(R.id.statusRoomMotion);
        luminosity = (TextView) findViewById(R.id.statusRoomLuminosity);

        statRoomName = (TextView) findViewById(R.id.statusRoomName);

        mHandler = new Handler();
        startRepeatingTask();
    }

    public void onClick(View view) {
        Intent intent = new Intent();
        setResult(RESULT_OK, intent);
        finish();
    }

    private int mInterval = 1000; // 5 seconds by default, can be changed later
    private Handler mHandler;

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopRepeatingTask();
    }

    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            try {
                // updateStatus();
            } finally {
                mHandler.postDelayed(mStatusChecker, mInterval);

                temp.setText((int)(Math.random()*2) + 20 +" C°");
                humid.setText((int)(Math.random()*4) + 48 +" %");
                co2.setText((String.format("%.2f", (Math.random()*1 + 3)))+" %");
                motion.setText((int)Math.round(Math.random()) == 1 ? "Ano" : "Ne");
                luminosity.setText(String.format("%.2f lux", (Math.random()*5) + 432));

                statRoomName.setText(MainActivity.name);
            }
        }
    };
    void startRepeatingTask() {
        mStatusChecker.run();
    }

    void stopRepeatingTask() {
        mHandler.removeCallbacks(mStatusChecker);
    }
}