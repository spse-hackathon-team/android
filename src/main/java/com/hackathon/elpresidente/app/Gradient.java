package com.hackathon.elpresidente.app;

import android.graphics.Color;

public class Gradient {
    public float min = 15.0f;
    public float max = 27.0f;
    public float rozsah;
    float ratio;

    public Gradient(){

    }
    public float count(float value){
        return Map(min, value, max, 0f, 240f);
    }

    public float Map(float min, float value, float max, float minOut, float maxOut){
        float diff = max - min;
        float ratio = (value-min)/diff;
        float diff2 = maxOut - minOut;
        float foo = diff2*ratio;
        float foo2 = minOut+foo;
        /*if(foo2<minOut){
            return minOut;
        }else if(foo2 > maxOut){
            return maxOut;
        }else return foo2;
        */
        if(foo2<120){
            float foo3 = 120f - foo2;
            return (foo3+120f);
        }else if(foo2>120f){
            float foo4 = foo2 - 120f;
            return (120f-foo4);
        }else return foo2;
    }
}
