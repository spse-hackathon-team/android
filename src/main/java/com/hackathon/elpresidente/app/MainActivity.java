package com.hackathon.elpresidente.app;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Button;

import org.w3c.dom.Text;

public class MainActivity extends AppCompatActivity {

    private Button livingBtn;
    private Button kitchenBtn;
    private Button room1Btn;
    private Button room2Btn;
    private Button bathroomBtn;
    private Button hallBtn;

    Gradient gradient = new Gradient();

    public Room livingRoom = new Room(0,"Obývací pokoj", true, true,true, true);
    public Room kitchen = new Room(0,"Kuchyň", true, true,true, true);
    public Room room1 = new Room(0,"Ložnice", true, true,true, true);
    public Room room2 = new Room(0,"Dětský pokoj", true, true,true, true);
    public Room bathroom = new Room(0,"Koupelna", true, true,true, true);
    public Room hall = new Room(0,"Chodba", true, false,true, false);

    public Room room;
    public static String name;

    //begin
    //=====================
    private int mInterval = 5000; // 5 seconds by default, can be changed later
    private Handler mHandler;

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopRepeatingTask();
    }

    Runnable mStatusChecker = new Runnable() {
        @Override
        public void run() {
            try {
               // updateStatus();
            } finally {
                mHandler.postDelayed(mStatusChecker, mInterval);

                hall.temp = (float)Math.floor(Math.random()*3) + 19;
                livingRoom.temp = (float)Math.floor(Math.random()*3) + 20;
                kitchen.temp = (float)Math.floor(Math.random()*3) + 21;
                bathroom.temp = (float)Math.floor(Math.random()*3) + 24;
                room1.temp = (float)Math.floor(Math.random()*3) + 22;
                room2.temp = (float)Math.floor(Math.random()*3) + 22;

                hall.humidity = (float)Math.floor(Math.random()*30) + 30;
                livingRoom.humidity = (float)Math.floor(Math.random()*30) + 30;
                kitchen.humidity = (float)Math.floor(Math.random()*30) + 30;
                bathroom.humidity = (float)Math.floor(Math.random()*30) + 30;
                room1.humidity = (float)Math.floor(Math.random()*30) + 30;
                room2.humidity = (float)Math.floor(Math.random()*30) + 30;

                float coo = (float)(Math.random()*7) + 1;
                hall.co2 = coo;
                livingRoom.co2 = coo;
                kitchen.co2 = coo;
                bathroom.co2 = coo;
                room1.co2 = coo;
                room2.co2 = coo;

                hall.motion = false;
                livingRoom.motion = false;
                kitchen.motion = false;
                bathroom.motion = false;
                room1.motion = false;
                room2.motion = false;

                float lux = (float)Math.floor(Math.random()*400) + 100;
                hall.luminosity = lux;
                livingRoom.luminosity = lux;
                kitchen.luminosity = lux;
                bathroom.luminosity = lux;
                room1.luminosity = lux;
                room2.luminosity = lux;

                //uprava barev buttonu
                float[] array = {0f,1f,1f};
                int color;

                array[0] = gradient.count(hall.temp);
                color = (Color.HSVToColor(array));
                hallBtn.setBackgroundColor(color);

                array[0] = gradient.count(livingRoom.temp);
                color = (Color.HSVToColor(array));
                livingBtn.setBackgroundColor(color);

                array[0] = gradient.count(kitchen.temp);
                color = (Color.HSVToColor(array));
                kitchenBtn.setBackgroundColor(color);

                array[0] = gradient.count(bathroom.temp);
                color = (Color.HSVToColor(array));
                bathroomBtn.setBackgroundColor(color);

                array[0] = gradient.count(room1.temp);
                color = (Color.HSVToColor(array));
                room1Btn.setBackgroundColor(color);

                array[0] = gradient.count(room2.temp);
                color = (Color.HSVToColor(array));
                room2Btn.setBackgroundColor(color);

            }
        }
    };

    public void room1Click(View view){
        room = room1;
        name = room.name;
        Intent myIntent = new Intent(view.getContext(), StatusActivity.class);
        startActivityForResult(myIntent, 0);
    }
    public void room2Click(View view){
        room = room2;
        name = room.name;
        Intent myIntent = new Intent(view.getContext(), StatusActivity.class);
        startActivityForResult(myIntent, 0);
    }
    public void kitchenClick(View view){
        room = kitchen;
        name = room.name;
        Intent myIntent = new Intent(view.getContext(), StatusActivity.class);
        startActivityForResult(myIntent, 0);
    }
    public void hallClick(View view){
        room = hall;
        name = room.name;
        Intent myIntent = new Intent(view.getContext(), StatusActivity.class);
        startActivityForResult(myIntent, 0);
    }
    public void livingRoomClick(View view){
        room = livingRoom;
        name = room.name;
        Intent myIntent = new Intent(view.getContext(), StatusActivity.class);
        startActivityForResult(myIntent, 0);
    }
    public void bathroomClick(View view){
        room = bathroom;
        name = room.name;
        Intent myIntent = new Intent(view.getContext(), StatusActivity.class);
        startActivityForResult(myIntent, 0);
    }

    void startRepeatingTask() {
        mStatusChecker.run();
    }

    void stopRepeatingTask() {
        mHandler.removeCallbacks(mStatusChecker);
    }
    //end
    //===================
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:

                    return true;
                case R.id.navigation_dashboard:

                    return true;
                case R.id.navigation_notifications:

                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        livingBtn = (Button)findViewById(R.id.livingBtn);
        kitchenBtn = (Button)findViewById(R.id.kitchenBtn);
        room1Btn = (Button)findViewById(R.id.room1Btn);
        room2Btn = (Button)findViewById(R.id.room2Btn);
        bathroomBtn = (Button)findViewById(R.id.bathroomBtn);
        hallBtn = (Button)findViewById(R.id.hallBtn);

        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        mHandler = new Handler();
        startRepeatingTask();
    }


}
