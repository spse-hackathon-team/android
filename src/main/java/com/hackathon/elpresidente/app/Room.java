package com.hackathon.elpresidente.app;

public class Room {
    public String name;
    public int id;
    public float temp, humidity, co2, luminosity;
    public boolean motion, light;

    public boolean hasTempSensor, hasLuminositySensor, hasMotionSensor, hasLightController;
    public boolean heater;

    public Room(int id, String name, boolean hasTempSensor,boolean hasLuminositySensor,boolean hasMotionSensor,boolean hasLightController){
        this.id = id;
        this.name = name;
        this.hasLightController = hasLightController;
        this.hasLuminositySensor = hasLuminositySensor;
        this.hasMotionSensor = hasMotionSensor;
        this.hasTempSensor = hasTempSensor;
    }
}
